local state = {}

--[[
	
]]

function state:init()
	-- initialize objects
	if not Calendar then
		Calendar = modules.Calendar({})
	end
	
	Calendar:resize(game.w, game.h)
end

function state:listen(bool)
	Calendar:listen(bool)
	
	if bool then
		if not state.events then
			state.events = {}
			state.events.kp = events("resize", function(w, h)
				Calendar:resize(w, h)
			end)
			state.events.mm = events("mousemoved", function(x, y, dx, dy)
				
			end)
		end
		
		for i, e in pairs(state.events) do
			e:register()
		end
	else
		
		for i, e in pairs(state.events) do
			e:remove()
		end
	end
end

function state:update(dt)
	Calendar:update(dt)
end

function state:draw()
	Calendar:draw()
end

return state