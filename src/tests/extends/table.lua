local args = ...

local suite = {
	desc = "Tests for the table.lua extend module.",
	tests = {
		{
			desc = "table.irandomchoice(t) returns a random value from the array portion of a table",
			run = function()
				local gota, gotb, gotc = 0, 0, 0
				local t = {}
				for i = 3, 1000 do
					t[tostring(i)] = 'c'
				end
				t[1] = 'a'
				t[2] = 'b'
				for j = 1, 1000 do
					local result = table.irandomchoice(t)
					if result == 'a' then
						gota = gota + 1
					elseif result == 'b' then
						gotb = gotb + 1
					elseif result == 'c' then
						gotc = gotc + 1
					else
						error('Got unexpected value: ' .. tostring(result))
					end
				end
				
				-- debug.log("irandomchoicechoice counts:\n" .. debug.tostring({
					-- gota = gota,
					-- gotb = gotb,
					-- gotc = gotc
				-- }))
				
				assert(gota > 0, "Expected to get 'c' at least once -- gota: " .. tostring(gota))
				assert(gotb > 0, "Expected to get 'b' at least once -- gotb: " .. tostring(gotb))
				assert(gotc == 0, "Didn't expect to get 'c' at all  -- getc: " .. tostring(gotc))
			end
		},
		{
			desc = "table.randomchoice(t) returns a random value from the entire table",
			run = function()
				local gota, gotb = 0, 0
				
				local t = {
					[1] = 'a',
					foo = 'b'
				}
				
				for j = 1, 1000 do
					local result = table.randomchoice(t)
					if result == 'a' then
						gota = gota + 1
					elseif result == 'b' then
						gotb = gotb + 1
					else
						error('Got unexpected value: ' .. tostring(result))
					end
				end
				
				-- debug.log("randomchoice counts:\n" .. debug.tostring({
					-- gota = gota,
					-- gotb = gotb
				-- }))
				
				assert(gota > 0, "Expected to get 'a' at least once -- gota: " .. tostring(gotb))
				assert(gotb > 0, "Expected to get 'b' at least once -- gotb: " .. tostring(gota))
			end
		},
		{
			desc = "table.weightedchoice(t) returns a weighted random value from table (array + assoc parts) where each value is a weight 0 or greater, and values add up to more than 0, each key is a possible choice",
			run = function()
				local gota, gotb, gotc = 0, 0, 0
				
				local t = {
					a = 0.5,
					b = 0.5,
					c = 0
				}
				
				for j = 1, 1000 do
					local result = table.weightedchoice(t)
					if result == 'a' then
						gota = gota + 1
					elseif result == 'b' then
						gotb = gotb + 1
					elseif result == 'c' then
						gotc = gotc + 1
					else
						error('Got unexpected value: ' .. tostring(result))
					end
				end
				
				-- debug.log("weightedchoice counts:\n" .. debug.tostring({
					-- gota = gota,
					-- gotb = gotb,
					-- gotc = gotc
				-- }))
				
				assert(gota > 0, "Expected to get 'c' at least once -- gota: " .. tostring(gota))
				assert(gotb > 0, "Expected to get 'b' at least once -- gotb: " .. tostring(gotb))
				assert(gotc == 0, "Didn't expect to get 'c' at all  -- getc: " .. tostring(gotc))
			end
		},
		{
			desc = "table.isarray(t) returns true for tables that have array components",
			run = function()
				local t1, t2, t3, v1 = {[1] = true}, {}, {z = true}, "z"
				assert(table.isarray(t1), "t1 expected to be array -- t1: " .. debug.tostring(t1))
				assert(table.isarray(t2), "t2 expected to be (empty) array -- t2: " .. debug.tostring(t2))
				assert(not table.isarray(t3), "t3 not expected to be array -- t3: " .. debug.tostring(t3))
				assert(not table.isarray(v1), "v1 not expected to be array -- v1: " .. debug.tostring(v1))
			end
		},
		{
			desc = "table.push(t, ...) pushes non-nil values to end of table",
			run = function()
				local t = {}
				local v1, v2, v3 = table.push(t, 1, nil, 2)
				local t2 = {
					v1 = v1,
					v2 = v2,
					v3 = v3
				}
				
				-- debug.log("t2:\n" .. debug.tostring(t2))
				-- debug.log("t(1):\n" .. debug.tostring(t))
				
				assert(t[1] == 1 and t[2] == 2, "table t expected to have [1] == 1 and [2] == 2 -- debug.tostring(t): " .. debug.tostring(t))
			end
		},
		{
			desc = "table.push(t, ...) returns pushed values",
			run = function()
				local t = {}
				local v1, v2, v3 = table.push(t, nil, 1, 'a')
				assert(v1 == nil and v2 == 1 and v3 == 'a', "Received unexpected values from table.push: " .. debug.tostring({v1 = v1, v2 = v2, v3 = v3}))
			end
		},
		{
			desc = "table.pop(t, ...) will remove all entries in t matching any arg",
			run = function()
				local t = {1, 2, 3, 1, 2, 3}
				local v1, v2, v3, v4 = table.pop(t, 2, 3)
				local args = {v1, v2, v3, v4}
				for k, v in pairs(args) do
					assert(v == 2 or v == 3, "expected to have only v == 2 or v == 3, v: " .. tostring(v))
				end
				assert(#args == 4, "expected to pop 4 values, args: " .. debug.tostring(args))
				assert(t[1] == 1 and t[2] == 1 and #t == 2, "expected t == {1, 1} -- t: " .. debug.tostring(t))
			end
		},
		{
			desc = "table.empty(t) will remove all k, v pairs from t",
			run = function()
				local t = {1, z = 'a'}
				local tp = table.empty(t)
				assert(not next(t), "expected t to be empty, t: " .. debug.tostring(t))
				assert(tp == t, "expected return from table.empty(t) to be same table as t, tp: " .. debug.tostring(tp))
			end
		},
		{
			desc = "table.extend(t, ...) copies supplied table key/value pairs to t (rightward tables overwriting leftward table entries)",
			run = function()
				local t1 = {a = 1, b = 2, c = 3}
				local t2 = {b = 5, c = 6}
				local t3 = {c = 9}
				local r = table.extend(t1, t2, t3)
				assert(r.a == 1 and r.b == 5 and r.c == 9, "expected return table == {a=1,b=5,c=9}, return table: " .. debug.tostring(r))
				assert(r == t1, "expected t1 to be same table as return table, return table: " .. debug.tostring(r))
			end
		},
		{
			desc = "table.shuffle(t) returns a shuffled copy of array part of table t",
			run = function()
				local t = {a = 1, b = 2}
				for i = 1, 1000 do
					t[i] = i
				end
				t[1002] = true
				
				local r = table.shuffle(t)
				assert(not r.a and not r.b, "didn't expect to get associative part of t, r: " .. debug.tostring(r))
				assert(r[1] ~= 1 and r[1000] ~= 1000, "didn't expect to have same first or last value in r after shuffling, r: " .. debug.tostring(r))
				assert(#r == 1000, "expected to have exactly 1000 values in r, #r: " .. tostring(#r))
			end
		},
		{
			desc = "table.sort2(t) will return a sorted copy of t",
			run = function()
				local t1 = {1, 3, 2}
				local t2 = table.sort2(t1)
				assert(t2[1] == 1, t2[2] == 2, t2[3] == 3, #t2 == 3, "expected t2 to be ascended sorted t1, t2: " .. debug.tostring(t2))
			end
		},
		{
			desc = "table.sort2(t[, string]) will return sorted copy of t that targets t[i][key] where string is key",
			run = function()
				local t1 = {
					{a = 1, b = 2, c = 3},
					{a = 1, b = 3, c = 5},
					{a = 1, b = 1, c = 1}
				}
				local t2 = table.sort2(t1, "c")
				assert(t2[1].b == 1, t2[2].b == 2, t2[3].b == 3, "expected t2 to be key sorted t1, t2: " .. debug.tostring(t2))
			end
		},
		{
			desc = "table.array(...) iterates the supplied iterator and returns array filled with values",
			run = function()
				local t1 = {1, 2, 3, 4, 5}
				local t2 = table.array(ipairs(t1))
				assert(t2[1] == 1 and t2[5] == 5 and #t2 == 5, "expected t2 to be like t1, t2: " .. debug.tostring(t2))
			end
		},
		{
			desc = "table.map(t, fn) applies fn to all t values and returns resulting table of values",
			run = function()
				local t1 = {a = 2, 4}
				local t2 = table.map(t1, function(v) return v*2 end)
				assert(t2.a == 4 and t2[1] == 8, "expected t2 = {a=4, 8}, t2: " .. debug.tostring(t2))
			end
		},
		{
			desc = "table.all(t[, fn]) returns true if all values or fn[values] are true, false otherwise",
			run = function()
				local t1 = {a = 2, 4}
				local test1 = table.all(t1)
				assert(test1 == true, "expected test1 == true, test1: " .. tostring(test1))
				local test2 = table.all(t1, function(v) return v == 2 end)
				assert(test2 == false, "expected test2 == false, test2: " .. tostring(test2))
			end
		},
		{
			desc = "table.any(t[, fn]) returns true if any value or fn[value] is true, false otherwise",
			run = function()
				local t1 = {a = 2, false}
				local test1 = table.any(t1)
				assert(test1 == true, "expected test1 == true, test1: " .. tostring(test1))
				local test2 = table.any(t1, function(v) return v == false end)
				assert(test2 == true, "expected test2 == true, test2: " .. tostring(test2))
				local test3 = table.any(t1, function(v) return v == 3 end)
				assert(test3 == false, "expected test3 == false, test3: " .. tostring(test3))
			end
		},
		{
			desc = "table.reduce(t, fn[, first]) processes t values left to right, starting at first, applying fn(a, b) to each consec values, returns final value",
			run = function()
				local t1 = {1, 2, 3}
				local v1 = table.reduce(t1, function(a, b) return a+b end)
				assert(v1 == 6, "expected v1==6, v1: " .. tostring(v1))
				
				local v2 = table.reduce(t1, function(a, b) return a+b end, 2)
				assert(v2 == 8, "expected v2==8, v2: " .. tostring(v2))
			end
		},
		{
			desc = "table.unique(t) returns a copy of t with duplicates removed",
			run = function()
				local t1 = {1, 2, 2, 1, 5, 5, 3, a = 'a', b = 'a'}
				t1 = table.unique(t1)
				local found = {[1] = false, [2] = false, [3] = false, [5] = false, a = false}
				for val, there in pairs(found) do
					for key, t1val in pairs(t1) do
						if t1val == val then
							if there then error("didn't expect to find " .. tostring(val) .. " twice in t1, t1: " .. debug.tostring(t1)) end
							found[val] = true
						end
					end
				end
				for k, v in pairs(found) do
					assert(v, "t1 value expected but not found: " .. tostring(v))
				end
			end
		},
		{
			desc = "table.filter(t, fn) returns a copy of table t that has only values where fn(value) is true",
			run = function()
				local function f(v)
					if type(v) == 'string' then
						return true
					end
					if v - 100 < 0 then return true end
					return false
				end
				
				local t1 = {5, 25, 100, 105, 'a', 'b'}
				local t2 = table.filter(t1, f)
				assert(table.find(t2, 5), "expected to keep 5 after filtering t1, t2: " .. debug.tostring(t2))
				assert(table.find(t2, 25), "expected to keep 25 after filtering t1, t2: " .. debug.tostring(t2))
				assert(table.find(t2, 'a'), "expected to keep 'a' after filtering t1, t2: " .. debug.tostring(t2))
				assert(table.find(t2, 'b'), "expected to keep 'b' after filtering t1, t2: " .. debug.tostring(t2))
				assert(not table.find(t2, 100), "expected to discard 100 after filtering t1, t2: " .. debug.tostring(t2))
				assert(not table.find(t2, 105), "expected to discard 105 after filtering t1, t2: " .. debug.tostring(t2))
				assert(#t2 == 4, "expected to keep 'b' after filtering t1, t2: " .. debug.tostring(t2))
			end
		},
		{
			desc = "table.filter(t, fn) returns a copy of table t that has only values where fn(value) is true",
			run = function()
				local function f(v)
					if type(v) == 'string' then
						return true
					end
					if v - 100 < 0 then return true end
					return false
				end
				
				local t1 = {5, 25, 100, 105, 'a', 'b'}
				local t2 = table.filter(t1, f, 'retainkeys')
				assert(t2[1] == 5, "expected to keep 5 after filtering t1, t2: " .. debug.tostring(t2))
				assert(t2[2] == 25, "expected to keep 25 after filtering t1, t2: " .. debug.tostring(t2))
				assert(t2[5] == 'a', "expected to keep 'a' after filtering t1, t2: " .. debug.tostring(t2))
				assert(t2[6] == 'b', "expected to keep 'b' after filtering t1, t2: " .. debug.tostring(t2))
				assert(t2[3] ~= 100, "expected to discard 100 after filtering t1, t2: " .. debug.tostring(t2))
				assert(t2[4] ~= 105, "expected to discard 105 after filtering t1, t2: " .. debug.tostring(t2))
			end
		},
		{
			desc = "table.reject(t, fn) returns a copy of table t that has only values where fn(value) is false",
			run = function()
				local function f(v)
					if type(v) == 'string' then
						return true
					end
					if v - 100 < 0 then return true end
					return false
				end
				
				local t1 = {5, 25, 100, 105, 'a', 'b'}
				local t2 = table.reject(t1, f)
				assert(not table.find(t2, 5), "expected to discard 5 after filtering t1, t2: " .. debug.tostring(t2))
				assert(not table.find(t2, 25), "expected to discard 25 after filtering t1, t2: " .. debug.tostring(t2))
				assert(not table.find(t2, 'a'), "expected to discard 'a' after filtering t1, t2: " .. debug.tostring(t2))
				assert(not table.find(t2, 'b'), "expected to discard 'b' after filtering t1, t2: " .. debug.tostring(t2))
				assert(table.find(t2, 100), "expected to keep 100 after filtering t1, t2: " .. debug.tostring(t2))
				assert(table.find(t2, 105), "expected to keep 105 after filtering t1, t2: " .. debug.tostring(t2))
				assert(#t2 == 2, "expected to keep 'b' after filtering t1, t2: " .. debug.tostring(t2))
			end
		},
		{
			desc = "table.reject(t, fn, retainkeys) returns a copy of table t that has only key/value pairs from t where fn(value) is false",
			run = function()
				local function f(v)
					if type(v) == 'string' then
						return true
					end
					if v - 100 < 0 then return true end
					return false
				end
				
				local t1 = {5, 25, 100, 105, 'a', 'b'}
				local t2 = table.reject(t1, f, 'retainkeys')
				assert(t2[1] ~= 5, "expected to keep 5 after filtering t1, t2: " .. debug.tostring(t2))
				assert(t2[2] ~= 25, "expected to keep 25 after filtering t1, t2: " .. debug.tostring(t2))
				assert(t2[5] ~= 'a', "expected to keep 'a' after filtering t1, t2: " .. debug.tostring(t2))
				assert(t2[6] ~= 'b', "expected to keep 'b' after filtering t1, t2: " .. debug.tostring(t2))
				assert(t2[3] == 100, "expected to discard 100 after filtering t1, t2: " .. debug.tostring(t2))
				assert(t2[4] == 105, "expected to discard 105 after filtering t1, t2: " .. debug.tostring(t2))
			end
		},
		{
			desc = "table.merge(...) returns table with given tables merged together, right-most table values have priority over left",
			run = function()
				local t1 = table.merge({a = 1, b = 2, c = 3}, {c = 8, d = 9})
				assert(t1.a == 1, "expected t1.a == 1, t1: " .. debug.tostring(t1))
				assert(t1.b == 2, "expected t1.b == 2, t1: " .. debug.tostring(t1))
				assert(t1.c == 8, "expected t1.c == 8, t1: " .. debug.tostring(t1))
				assert(t1.d == 9, "expected t1.d == 9, t1: " .. debug.tostring(t1))
			end
		},
		{
			desc = "table.append(...) returns new array consisting of given arrays concatenated",
			run = function()
				local t1 = table.append({1, 2}, {3, 4}, {5, 6})
				for i = 1, 6 do
					assert(t1[i] == i, "expectedt t1[" .. i .. "] == " .. i .. ", t1: " .. debug.tostring(i))
				end
			end
		},
		{
			desc = "table.find(t, value) returns index of value in t if found or nil",
			run = function()
				local k = 7
				local t2 = {}
				local t1 = {a = 1, 'a', [t2] = k}
				assert(table.find(t1, 1) == 'a', "expected t1['a'] == 1, t1: " .. debug.tostring(t1))
				assert(table.find(t1, 'a') == 1, "expected t1[1] == 'a', t1: " .. debug.tostring(t1))
				assert(table.find(t1, 7) == t2, "expected t1[t2] == 7, t1: " .. debug.tostring(t1))
			end
		},
		{
			desc = "table.match(t, fn) returns a key, val where fn(val) is true -- or nil",
			run = function()
				local t1 = {1, 5, 8, 7}
				local f = function(x) return x%2 == 0 end
				local val, key = table.match(t1, f)
				assert(val == 8 and key == 3, "Expected val, key to be 8 and 3, val, key: " .. tostring(val) .. ', ' .. tostring(key))
			end
		},
		{
			desc = "table.count(t[, fn]) counts values in t or times where fn(v) is true",
			run = function()
				local t1 = {1, 5, 8, 7}
				local f = function(x) return x%2 == 0 end
				local c1 = table.count(t1, f)
				local c2 = table.count(t1)
				assert(c1 == 1, "expected c1 == 1, c1: " .. tostring(c1))
				assert(c2 == 4, "expected c1 == 4, c1: " .. tostring(c1))
			end
		},
		{
			desc = "table.slice(t[, i[, j]]) is string.sub for arrays, returns array copied i to j",
			run = function()
				local t1 = {1, 5, 8, 7}
				local t2 = table.slice(t1, -3, -1)
				local t3 = table.slice(t1, 1, 2)
				
				assert(t2[1] == 5 and t2[3] == 7, "expected t2={5,8,7}, t2: " .. debug.tostring(t2))
				assert(t3[1] == 1 and t3[2] == 5, "expected t3={1, 5}, t3: " .. debug.tostring(t3))
			end
		},
		{
			desc = "table.first(t[, n]) returns the first or the first n elements in array t",
			run = function()
				local t = {1, 5, 8, 7}
				local a = table.first(t)
				local b, c = table.unpack(table.first(t, 2))
				assert(a == 1, "expected a == 1, a: " .. tostring(a))
				assert(b == 1, "expected b == 1, b: " .. debug.tostring(b))
				assert(c == 5, "expected c == 5, c: " .. tostring(c))
			end
		},
		{
			desc = "table.last(t[, n]) returns the first or the first n elements in array t",
			run = function()
				local t = {1, 5, 8, 7}
				local a = table.last(t)
				local b, c = table.unpack(table.last(t, 2))
				assert(a == 7, "expected a == 7, a: " .. tostring(a))
				assert(b == 8, "expected b == 8, b: " .. debug.tostring(b))
				assert(c == 7, "expected c == 7, c: " .. tostring(c))
			end
		},
		{
			desc = "table.invert(t) returns a copy of t with k, v pairs swapped",
			run = function()
				local t = {1, 5, 8, 7, 'a'}
				t = table.invert(t)
				assert(t[1] == 1, "expected t[1] == 1, t: " .. debug.tostring(t))
				assert(t[5] == 2, "expected t[5] == 2, t: " .. debug.tostring(t))
				assert(t[8] == 3, "expected t[8] == 3, t: " .. debug.tostring(t))
				assert(t[7] == 4, "expected t[7] == 4, t: " .. debug.tostring(t))
				assert(t['a'] == 5, "expected t['a'] == 5, t: " .. debug.tostring(t))
			end
		},
		{
			desc = "table.pick(t, ...) returns a copy of t filtered by keys ...",
			run = function()
				local t = {1, 5, 8, 7, z = 'a'}
				t = table.pick(t, 3, 4, 'z')
				assert(t[3] == 8, "expected t[3] == 8, t: " .. debug.tostring(t))
				assert(t[4] == 7, "expected t[4] == 7, t: " .. debug.tostring(t))
				assert(t.z == 'a', "expected t.z == 'a', t: " .. debug.tostring(t))
			end
		},
		{
			desc = "table.keys(t) returns an array containing the keys of t",
			run = function()
				local t = {1, 5, 8, z = 'a'}
				local keys = table.keys(t)
				assert(table.find(keys, 1), "expected to find [1] in keys, keys: " .. debug.tostring(keys))
				assert(table.find(keys, 2), "expected to find [2] in keys, keys: " .. debug.tostring(keys))
				assert(table.find(keys, 3), "expected to find [3] in keys, keys: " .. debug.tostring(keys))
				assert(table.find(keys, 'z'), "expected to find 'z' in keys, keys: " .. debug.tostring(keys))
			end
		},
		{
			desc = "table.clone(t) returns a copy of t",
			run = function()
				local t1 = {1, 2, 3, 4, 5, a = 'z'}
				local t2 = table.clone(t1)
				assert(t2[1] == 1 and t2[5] == 5 and #t2 == 5 and t2.a == 'z', "expected t2 to be like t1, t2: " .. debug.tostring(t2))
			end
		},
	}
}

return suite