local args = ...

local suite = {
	desc = "Tests for the string.lua extend module.",
	tests = {
		{
			desc = "string.split(str[, sep]) returns an array of words in str or where sep is delimiter. consec delims yield empty string ''",
			run = function()
				local t = string.split("a b c")
				assert(t[1] == 'a', "expected t[1] == 'a' from string split result, t: " .. debug.tostring(t))
				assert(t[3] == 'c', "expected t[3] == 'c' from string split result, t: " .. debug.tostring(t))
				t = string.split("a||b||c", "|")
				assert(t[1] == 'a', "expected t[1] == 'a' from string split result, t: " .. debug.tostring(t))
				assert(t[2] == '', "expected t[2] == '' from string split result, t: " .. debug.tostring(t))
				assert(#t == 5, "expected #t == 5 from string split result, t: " .. debug.tostring(t))
			end
		}
		,{
			desc = "string.trim(str, chars) returns str trimmed of whitespace or all chars in [chars] instead",
			run = function()
				local abc = " abc "
				local res = string.trim(abc)
				assert(res == "abc", "expected to trim whitespace, res: " .. debug.tostring(res))
				abc = "-abc-"
				res = string.trim(abc, "-")
				assert(res == "abc", "expected to trim '-', res: " .. debug.tostring(res)) 
			end
		}
		,{
			desc = "string.template(str, vars) returns string where vars can be table and {key} designates key in str template",
			run = function()
				local str = "{1} {2} and {final}"
				local vars = {
					'a',
					'b',
					final = 'c'
				}
				local res = string.template(str, vars)
				assert(res == "a b and c", "expected a b and c, res: " .. debug.tostring(res))
			end
		}
		,{
			desc = "string.uuid() returns a unique string id (version 4 of RFC 4122 : https://www.ietf.org/rfc/rfc4122.txt)",
			run = function()
				local id = string.uuid()
				assert(id:len() == 36, "expected 36 character uuid, id: " .. debug.tostring(id))
			end
		}
	}
}

return suite