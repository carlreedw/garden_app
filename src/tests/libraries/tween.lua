local args = ...
local suite = {
	desc = "Tests for tween library",
	tests = {
		{
			desc = "Tweens a target table's value(s) to final table's value(s) in time. tween.done is true when tween is done",
			run = function()
				local t = {val = 0}
				local f = {val = 1}
				local tw = tween(t, f)	-- assume 1 s tween time
				tw:update(1)
				assert(t.val == 1, "Tween didn't change target table value")
				assert(tw.done, "Tween didn't mark public .done property upon completing tween")
			end
		},{
			desc = "tween stops adjusting target's values upon completion",
			run = function()
				local t = {val = 0}
				local f = {val = 1}
				local tw = tween(t, f)	-- assume 1 s tween time
				tw:update(1)
				tw:update(.1)
				assert(t.val == 1, "Tween value incorrect -- likely kept updating target values")
			end
		},{
			desc = "Tween can take a specified duration of time to complete",
			run = function()
				local t = {val = 0}
				local f = {val = 1}
				local tw = tween(t, f, 1.5)
				tw:update(1.4)
				assert(t.val ~= 1.5, "Tween value is correct too early")
				tw:update(.1)
				
				assert(t.val == 1, "Tween value is not correct on time -- t.val: " .. tostring(t.val))
			end
		},{
			desc = "Tween functions return tween as first value to allow chaining",
			run = function()
				local t = {val = 0}
				local f = {val = 1}
				local tw = tween(t, f)
				local fnames = {'update', 'oncomplete', 'after', 'before', 'delay', 'ease', 'finish', 'reset'}
				for i, fname in ipairs(fnames) do
					if not tw[fname] then error("tween doesn't have function: " .. fname) end
					assert(tw == tw[fname](tw), fname .. ": didn't return tween as first return value to allow chaining")
				end
			end
		},{
			desc = "You can specify a callback for tween to trigger upon tween completion",
			run = function()
				local t = {val = 0}
				local f = {val = 1}
				local tw = tween(t, f)
				
				local v1, v2
				local foo = function(a, b)
					v1 = true
					v2 = a + b
				end
				
				tw:oncomplete(foo, 1, 2)
				
				tw:update(1)
				assert(v1 == true, "tween function specified as callback didn't get called upon tween completion")
				assert(v2 == 3, "tween function specified for :oncomplete as didn't use supplied function arguments")
			end
		},{
			desc = "You can specify a callback for tween to trigger before and/or after each update",
			run = function()
				local t = {val = 0}
				local f = {val = 1}
				local tw = tween(t, f)
				
				local before = function(arg)
					assert(arg == 'a', "argument not passed correctly to :before callback")
					assert(t.val == 0, "target value was tweened before :before callback was called")
				end
				
				local after = function(arg)
					assert(arg == 'a', "argument not passed correctly to :before callback")
					assert(t.val == 1, "target value was not tweened before :after callback was called")
				end
				
				tw:before(before, 'a')
				tw:after(after, 'a')
				
				tw:update(1)
			end
		},{
			desc = "You can reset a tween at any time to restore the value to target table to initial (when tween was created)",
			run = function()
				local t = {val = 0}
				local f = {val = 1}
				local tw = tween(t, f)
				
				tw:update(.5)
				assert(t.val ~= 0, "tween target table value never changed after :update")
				tw:reset()
				assert(t.val == 0, "tween target table value didn't reset after call to :reset")
			end
		},{
			desc = "Tweens can be delayed for duration before they begin tweening target table values",
			run = function()
				local t = {val = 0}
				local f = {val = 1}
				local tw = tween(t, f)
				tw:delay(1)
				
				tw:update(1)
				assert(t.val == 0, "tween target table value changed before delay was completed")
				tw:update(1)
				assert(t.val == 1, "tween target table value didn't tween after :delay and subsequent :update")
			end
		},{
			desc = "Tweens can finish early tweening the target table's values to final values at any time by calling :finish",
			run = function()
				local t = {val = 0}
				local f = {val = 1}
				local tw = tween(t, f)
				
				local v
				local foo = function()
					v = true
				end
				
				tw:oncomplete(foo)
				tw:finish()
				
				assert(v, "tween didn't callback :oncomplete function upon calling :finish")
				assert(t.val == 1, "tween didn't finish tweening target table value")
			end
		},{
			desc = "Tweens can use an easing function to change how the target table value is tweened to it's final value",
			run = function()
				local t = {val = 0}
				local f = {val = 1}
				local tw = tween(t, f)
				
				tw:ease('quad')
				tw:update(.5)
				assert(t.val == .25, "tween didn't use quadratic easing function via string spec as expected, t.val == " .. tostring(t.val))
				
				tw:reset()
				tw:ease(function(x) return x*x*x end)
				tw:update(.5)
				assert(t.val == .5*.5*.5, "tween didn't use custom easing function as expected")
				
				tw:reset()
				tw:ease('quad', true)
				tw:update(.25)
				local expected = 1 - (1 - .25) * (1 - .25)
				assert(t.val == expected, "tween didn't invert easing function as expected")
			end
		},{
			desc = "Tweens are independent",
			run = function()
				local t1 = {val1 = 0}
				local t2 = {val2 = 0}
				local tw1 = tween(t1, {val1 = 1})
				local tw2 = tween(t2, {val2 = 10})
				tw1:update(.5)
				tw1:update(.5)
				tw2:update(.5)
				tw2:update(.5)
				assert(t1.val1 == 1, "tween target (t1.val1) not equal to 1: " .. tostring(t1.val1))
				assert(t2.val2 == 10, "tween target (t2.val2) not equal to 10: " .. tostring(t2.val2))
			end
		}
	}
}

return suite