local args = ...

local tbl = {
	foo = function(a) return a + 1 end,
	bar = function(b) return b end
}

local suite = {
	desc = "Tests for events library",
	tests = {
		{
			desc = "You may insert a hook into an existing function and create an associated event which fires a callback when the existing function is called.",
			run = function()
				events:hook(tbl, "foo")
				local sum = 0
				local event = events("foo", function(arg) sum = sum + arg end):register()
				local b = tbl.foo(2)
				assert(b == 3, "Hooked function did not execute correctly.")
				assert(sum == 2, "Event callback function did not execute correctly.")
				events:unhook('foo')
			end
		},
		{
			desc = "You may unhook a callable from the events registry to prevent event triggering.",
			run = function()
				events:hook(tbl, "bar")
				local sum = 0
				local event = events("bar", function(arg) sum = sum + arg end):register()
				local b = tbl.bar(2)
				assert(b == 2, "Hooked function did not execute correctly.")
				events:unhook("bar")
				event = nil
				local c = tbl.bar(5)
				assert(c == 5, "Hooked function did not execute correctly after unhooking.")
				assert(sum == 2, "Event callback didn't fire only once as test requires. sum: " .. tostring(sum))
			end
		},
		{
			desc = "You may trigger events with [name] by calling events:trigger(name, ...)",
			run = function()
				events:hook(tbl, "foo")
				local sum = 0
				local event = events("foo", function(arg) sum = sum + arg end):register()
				events:trigger('foo', 2)
				assert(sum == 2, "Event didn't trigger correctly.")
				events:unhook('foo')
			end
		},
		{
			desc = "You may cancel a listener event with event:remove()",
			run = function()
				events:hook(tbl, "foo")
				local sum = 0
				local event1 = events("foo", function(arg) sum = sum + arg end):register()
				local event2 = events("foo", function(arg) sum = sum + arg end):register()
				local b = tbl.foo(1)
				assert(sum == 2, "Event callback function did not execute correctly.")
				event2:remove()
				local b = tbl.foo(1)
				assert(sum == 3, "Event callback function did not execute correctly.")
				events:unhook("foo")
			end
		},
		{
			desc = "You may prioritize event listeners",
			run = function()
				events:hook(tbl, "bar")
				local abc = ""
				local event1 = events("bar", function(arg) abc = abc .. "b" end, 0):register()
				local event2 = events("bar", function(arg) abc = abc .. "a" end, 1):register()
				
				tbl.bar()
				assert(abc == "ab", "Events did not fire in priority order -- abc: " .. tostring(abc))
				event1:prioritize(2)
				
				tbl.bar()
				assert(abc == "abba", "Events did not re-prioritize correctly -- abc: " .. tostring(abc))
				events:unhook("bar")
			end
		}
	}
}

return suite