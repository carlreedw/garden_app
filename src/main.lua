--[[
PROJECT: garden_app
AUTHORS: carl
DATE: 2022-08-20
DESC: garden_app prototype
ABLE: "D:/love2d/11.4/love.exe" "C:/dev/garden_app/src"
SIK4ULAH: "C:/dev/love/11.4/love.exe" "C:/dev/garden_app/src"
--]]

function love.load()
	lg = love.graphics
	game = love.filesystem.load('game.lua')({
		run_tests = true,
		-- load_logging = true
	})
	
	game.events.set_wh_on_resize = events('resize', function(w, h)
		game.w, game.h = w, h
	end)
	game.events.set_wh_on_resize.callback(lg.getDimensions())
	
	-- enable key repeat
	lk.setKeyRepeat(true)
	
	-- enable quit on esc and debug.debug when press debug.debug_key (f10 default)
	debug.listen(true)
	game:listen(true)
	
	game:start()
end

function love.update(dt)
	game:update(dt)
end

function love.draw()
	game:draw()
end