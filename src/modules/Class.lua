-- class table
local Class = {}

-- created so that instances of Class use the class's functions (like :draw, if I had added that in this file)
local instanceMetatable = {
	__index = Class
}

function Class.new(Class, settings)
	if settings then
		assert(type(settings) == 'table', "Class constructor requires argument to be settings table")
	end
	local self = settings
	setmetatable(self, instanceMetatable)
	
	
	
	return self
end

function Class:report()
	print('Class reporting - self.name = ' .. tostring(self.name))
end

-- created so that calling the Class table (like Class({x = 50, y = 100}) or whatever), returns a new instance of Class
local ClassMetatable = {
	__call = Class.new
}
setmetatable(Class, ClassMetatable)

-- return class table
return Class