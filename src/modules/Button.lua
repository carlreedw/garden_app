-- class table
local Button = {}

-- created so that instances of Button use the class's functions (like :draw, if I had hadded that in this file)
local instanceMetatable = {
	__index = Button
}

function Button.new(Button, settings)
	if settings then
		assert(type(settings) == 'table', "Button constructor requires argument to be settings table")
	end
	local instance = settings
	setmetatable(instance, instanceMetatable)
	
	instance.x = instance.x or 0
	instance.y = instance.y or 0
	instance.w = instance.w or 128
	instance.h = instance.h or 32
	instance.mousePressListener = events('mousepressed', function(x, y, button, istouch, presses)
		if instance:hover(x, y) then
			instance.pressed = true
		end
	end)
	instance.mouseReleaseListener = events('mousereleased', function(x, y, button, istouch, presses)
		if instance.pressed then
			instance.action()
		end
		instance.pressed = false
	end)
	
	if not instance.font then
		instance:resizeFont()
	end
	
	return instance
end
-- created so that calling the Button table (like Button({x = 50, y = 100}) or whatever), returns a new instance of Button
local ButtonMetatable = {
	__call = Button.new
}
setmetatable(Button, ButtonMetatable)

---- class methods

function Button:draw()
	local bx, by, bw, bh = self:getBounds()
	local lg = love.graphics
	local hover = self:hover(game:getMousePosition())
	local pressed = self.pressed
	
	-- draw bg
	lg.setColor(pressed and {0.15, 0.15, 0.15, 0.95} or hover and {1.0, 1.0, 1.0, 0.25} or {0.5, 0.5, 0.5, 0.25})
	lg.rectangle('fill', bx - bw*0.5, by - bh*0.5, bw, bh)
	
	-- draw outline
	lg.setColor(pressed and {0.7, 0.72, 0.75} or hover and {1, 1, 1} or {0.8, 0.94, 0.87})
	lg.setLineWidth(bw*0.01)
	lg.rectangle('line', bx - bw*0.5, by - bh*0.5, bw, bh)
	
	-- print label
	if self.label then
		local lw = self.font:getWidth(self.label)
		lg.setFont(self.font)
		lg.print(self.label, bx - lw*0.5, by - 0.25*bh)
	end
end

function Button:listen(bool)
	if bool then
		self.mousePressListener:register()
		self.mouseReleaseListener:register()
	else
		self.mousePressListener:remove()
		self.mouseReleaseListener:remove()
	end
end

function Button:getBounds()
	local bx, by, bw, bh = self.x, self.y, self.w, self.h
	return bx, by, bw, bh
end

function Button:hover(mx, my)
	local bx, by, bw, bh = self:getBounds()
	if mx <= (bx + bw*0.5) and mx >= (bx - bw*0.5) and my <= (by + bh*0.5) and my >= (by - bh*0.5) then
		return true
	end
	return false
end

function Button:resizeFont()
	local bx, by, bw, bh = self:getBounds()
	local font_size = math.round(bh*0.5)
	font_size = math.clamp(4, font_size, 120)
	self.font = love.graphics.newFont(font_size)
end


-- return class table
return Button