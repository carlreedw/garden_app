-- class table
local Calendar = {}

-- created so that instances of Calendar use the class's functions (like :draw, if I had added that in this file)
local instanceMetatable = {
	__index = Calendar
}

function Calendar.new(Calendar, settings)
	if settings then
		assert(type(settings) == 'table', "Calendar constructor requires argument to be settings table")
	end
	local self = settings
	setmetatable(self, instanceMetatable)
	
	if not Calendar.month_font then
		Calendar.month_font = lg.newFont(math.round(game.h * 0.0815))
	end
	
	self.today_info = os.date("*t", os.time())
	debug.log(inspect(self.today_info))
	
	self.month_label = os.date("%B")
	
	return self
end

function Calendar:draw()
	-- draw calendar outline
	local x, y, w, h = self.x, self.y, self.w, self.h
	lg.setColor(1, 1, 1)
	lg.rectangle('fill', x, y, w, h)
	
	-- print month header
	if self.month_label then
		lg.setColor(.1, .1, .1)
		lg.setFont(self.month_font)
		local tw = self.month_font:getWidth(self.month_label)
		lg.print(self.month_label, (game.w - tw)/2, game.h * 0.07)
	end
	
	-- draw 5 rows of 7 squares
	lg.setColor(.1, .1, .1)
	local x_border, y_border = game.w * 0.12/2, game.h * 0.125
	local row_height = game.h / 8
	local col_width = (game.w - x_border * 2) / 7
	lg.rectangle('line', x_border, y_border + row_height, game.w - x_border * 2, row_height * 5)
	for i = 1, 5 do
		-- horizontal rectangles
		lg.rectangle('line', x_border, y_border + i * row_height, game.w - x_border * 2, row_height)
		for j = 1, 6 do
			-- vertical rectangles
			lg.rectangle('line', x_border + j * col_width, y_border + row_height, col_width, row_height * 5)
		end		
	end
	
	-- print date numbers
	lg.setColor(0.2, 0.2, 0.2, .88)
	
	
end

function Calendar:update(dt)
	
end

function Calendar:listen(bool)
	
end

function Calendar:resize(w, h)
	local x_border, y_border = w * 0.08/2, h * 0.08/2
	self.x, self.y = x_border, y_border
	self.w = w - x_border * 2
	self.h = h - y_border * 2
	Calendar.month_font = lg.newFont(math.round(game.h * 0.0815))
end

-- created so that calling the Calendar table (like Calendar({x = 50, y = 100}) or whatever), returns a new instance of Calendar
local ClassMetatable = {
	__call = Calendar.new
}
setmetatable(Calendar, ClassMetatable)

-- return class table
return Calendar