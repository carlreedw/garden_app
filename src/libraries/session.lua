
-- session library

--[[
	a note on overriding session.read and session.write
	session.read receives arguments:
		line (string) (the next line from the event log loaded)
	and returns	(timestamp, eventname, ...) where ... is a variable number of args
	
	session.write receives arguments:
		timestamp (number) (this is the current value of session's internal .timer property that gets updated via :update)
		eventname (string) (this is the key that triggers a particular event in events library)
		... (any number of arguments that will be the args sent to the events:trigger call upon reading)
	and returns a string (line) which is written to the event log file
]]

local events_lib = events

local session = {
	print = function() end,
	default_events = {'keypressed', 'keyreleased', 'mousepressed', 'mousereleased', 'mousemoved', 'wheelmoved'}
}

function session:load(filename)
	if not love.filesystem.getInfo(filename) then
		error("Tried to load a session file but file with name: " .. filename .. " -- doesn't exist", 2)
	end
	
	self:stop()
	self.timer = 0
	self.filename = filename
	self.mode = 'playing'
	
	-- get the next event we can be waiting for
	coroutine.resume(self.routine)
	if not self.next or not self.next.ts then
		self.print("loaded an empty session file: " .. filename .. " -- traceback:\r\n" .. debug.traceback().. "\r\n")
		self:stop()
	end
end

function session:record(filename, argtable)
	-- check args for validity
	assert(type(filename) == 'string', "filename must be a string")
	local argtable = argtable
	if argtable then
		assert(type(argtable) == 'table', "2nd argument must be a table if present")
	else
		argtable = {}
	end
	
	-- if recording, save to file, if playing, stop
	self:stop()
	
	-- store some metadata about our recording
	self.metadata = {}
	local mdata = self.metadata
	mdata.game = self.game or argtable.game or "template"
	mdata.version = self.version or argtable.version or "[version -- write sha256 commit shortcode here]"
	mdata.date = os.date("%Y-%m-%dT%X")
	mdata.start_timestamp = love.timer.getTime()
	
	-- create file
	local success, err = love.filesystem.newFile(filename, 'w')
	if success then
		self.filename = filename
		self.file = success
	else
		-- self.print("error creating new file: " .. err)
		error("error creating new file: " .. err, 2)
		self:stop()
		return
	end
	
	-- create events
	self.events = {}
	for i, eventName in ipairs(argtable.events or self.default_events) do
		self.events[eventName] = events_lib(eventName, function(...)
			if self.paused then return nil end
			local line = self.write(self.timer, eventName, ...)
			if not line then return end
			self.file:write(line)
		end):register()
	end
	
	-- set timer, mode
	self.timer = 0
	self.mode = 'recording'
	self.print("started recording session to file: " .. self.filename)
end

function session:play()
	-- if paused, resume recording/playing
	self.paused = nil
end

function session:pause()
	-- set property so :update doesn't do anything
	self.paused = true
end

function session:stop()
	if self.mode == 'recording' then
		-- remove events
		local event_names = {}
		for name, event in pairs(self.events) do
			event:remove()
			table.insert(event_names, name)
		end
		local eventstring = table.concat(event_names, ", ")
		-- format metadata
		local tablestring = "{\r\n"
		tablestring = tablestring .. "\tgame = " .. self.metadata.game .. ",\r\n"
		tablestring = tablestring .. "\tversion = " .. self.metadata.version .. ",\r\n"
		tablestring = tablestring .. "\tdate = " .. self.metadata.date .. ",\r\n"
		tablestring = tablestring .. "\tevents = {" .. eventstring .. "}\r\n"
		tablestring = tablestring .. "}"
		
		-- write metadata
		self.file:write(tablestring)
		self.file:close()
		self.print("stopped recording, saved session to file: " .. self.filename)
	end
	
	-- from recording
	self.metadata, self.filename, self.file, self.events, self.timer, self.mode = nil
	
	-- from playing
	self.next = nil
	
	-- remake coroutine since it's probably half way through some file somewhere
	self:makeCoroutine()
end

function session:update(dt)
	if self.paused then return end
	if not self.mode then return end
	
	self.timer = self.timer + dt
	if self.mode == 'playing' then
		if self.timer < self.next.ts then
			return
		end
		
		-- convert numeric args to number
		for i, a in ipairs(self.next.args) do
			if tonumber(a) then self.next.args[i] = tonumber(a) end
		end
		events_lib:trigger(self.next.eventname, unpack(self.next.args))
		
		-- load self.next with the next line (ts, eventname, args...) from the session file
		coroutine.resume(self.routine)
		
		if coroutine.status(self.routine) == 'dead' then
			self.print('finished playing back event log from file: ' .. self.filename)
			self:stop()
		end
	end
end

function session:makeCoroutine()
	self.routine = coroutine.create(function()
		for line in love.filesystem.lines(self.filename) do
			if line == "{" or line == "{\r\n" then
				break
			end
			
			local tokens = {self.read(line)}
			self.next = {args = {}}
			if tokens[1] then self.next.ts = tokens[1] end
			if tokens[2] then self.next.eventname = tokens[2] end
			for i = 3, #tokens do
				table.insert(self.next.args, tokens[i])
			end
			
			coroutine.yield()
		end
	end)
end

-- the following two functions may be overridden to extend/modify session behavior

function session.read(line)
	local ts, eventname, args
	args = {}
	
	-- assume first, 2nd tokens are ts and eventname, rest are args
	for token in string.gmatch(line, '[^,]+') do
		local token = string.trim(token)
		if tonumber(token) then token = tonumber(token) end
		
		if not ts then
			ts = token
		elseif not eventname then
			eventname = token
		else
			args[#args + 1] = token
		end
	end
	
	return ts, eventname, unpack(args)
end

function session.write(timestamp, eventname, ...)
	local linestring = timestamp .. ", " .. eventname
	for _, v in ipairs({...}) do
		linestring = linestring .. ", " .. tostring(v)
	end
	return linestring .. "\r\n"
end

-- metatables to implement instantiation and inheritance

local instance_mt = {
	__index = session
}

local class_mt = {
	__call = function(class)
		local session_instance = {}

		setmetatable(session_instance, instance_mt)
		session_instance:makeCoroutine()
		
		return session_instance
	end
}

setmetatable(session, class_mt)

return session