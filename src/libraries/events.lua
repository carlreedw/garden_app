local events = {}

events.registry = {}

local function isCallable (value)
	return type(value) == 'function' or getmetatable(value) and getmetatable(value).__call
end

local function makehook(t, key)
	local original = t[key]
	
	-- assert(not events.registry[key], "already registered an event hook for key: " .. tostring(key))
	if events.registry[key] then print("already registered an event hook for key: " .. tostring(key)) end
	
	events.registry[key] = {
		table = t,
		key = key,
		original = original
	}
	-- debug.log('writing events registry for key: ' .. tostring(key))
	
	if isCallable(original) then
		t[key] = function (...)
			events:trigger(key, ...)
			return original(...)
		end
	else
		t[key] = function (...)
			events:trigger(key, ...)
		end
	end
end

function events:hook(t, keys)
	if type(keys) == 'table' then
		for k, v in pairs(keys) do
			makehook(t, v)
		end
	elseif keys then
		makehook(t, keys)
	else
		for k, v in pairs(t) do
			makehook(t, k)
		end
	end
end

function events:unhook(key)
	if key then
		local reg = events.registry[key]
		
		if not reg then print("no registered event hook for key: " .. tostring(key)) end
		
		-- restore original function (remove inserted trigger hook)
		reg.table[reg.key] = reg.original
		
		for i, handler in ipairs(reg) do
			handler:remove()
		end
		events.registry[key] = nil
	else
		-- unhook all
		for name, reg in pairs(events.registry) do
			reg.table[reg.key] = reg.original
			for i, h in ipairs(reg) do
				h:remove()
			end
		end
		events.registry = {}
	end
end

function events:trigger(key, ...)
	local reg = events.registry[key]
	if not reg then
		error("in trigger for tostring(key): " .. tostring(key) .. "\n" .. debug.traceback())
	end
	assert(reg, "no registered event hook for key: " .. tostring(key))
	for i, handler in ipairs(reg) do
		handler.returns = {handler.callback(...)}
	end
end

local newevent = function(events, key, callback, priority)
	local reg = events.registry[key]
	assert(reg, "no registered event hook for key: " .. tostring(key))
	
	local sortf = function(a, b) return a.priority > b.priority end
	
	local event = {
		name = key,
		callback = callback,
		priority = priority or 0
	}
	function event:register()
		if self.registered then return end
		self.registered = true
		reg[#reg + 1] = self
		table.sort(reg, f or sortf)
		return self
	end
	function event:remove()
		if not self.registered then return end
		self.registered = nil
		local index
		for i, h in ipairs(reg) do
			if h == self then
				index = i
				break
			end
		end
		if index then
			table.remove(reg, index)
			table.sort(reg, f or sortf)
		end
		return self
	end
	function event:prioritize(z, f)
		self.priority = z or 0
		table.sort(reg, f or sortf)
		return self
	end
	
	return event
end

local mt = {
	__call = newevent
}
setmetatable(events, mt)

return events