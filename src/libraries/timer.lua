local timer = {}

local function is_callable(x)
	if type(x == 'function') then return true end
	local mt = getmetatable(x)
	if mt.__call then return true end
	return false
end

setmetatable(timer, {__call = function(class, tbl)
	-- tbl options: delay, period, action, args, undo, undo_args
	local instance = {
		label = 'timer',
		trigger_count = 0,
		recent_trigger_count = 0,
		undo_count = 0,
		recent_undo_count = 0,
		elapsed = 0
	}
	
	setmetatable(instance, {__index = timer})
	instance.__call = nil
	
	if tbl then
		assert(type(tbl) == 'table', "If you supply an argument to timer(), arg must be a table with timer parameters")
		assert(tbl.delay or tbl.period, "If you supply a parameter table to timer(), it must specify a delay (for :after) or period (for :every)")
		assert(tbl.action and is_callable(tbl.action), "If you supply a parameter table to timer(), it must include a callable 'action' parameter")
		if tbl.period then
			instance:every(tbl.period, tbl.action, (unpack or table.unpack)(tbl.args or {}))
		else
			instance:after(tbl.delay, tbl.action, (unpack or table.unpack)(tbl.args or {}))
		end
		if tbl.undo then
			assert(is_callable(tbl.undo), "If you supply a parameter table to timer() that has an undo value, undo must be a function or callable table")
			instance:setundo(tbl.undo, (unpack or table.unpack)(tbl.undo_args or {}))
		end
	end
	
	return instance
end})

function timer:after(delay, f, ...)
	assert(type(delay) == 'number', "First arg supplied to timer:after() must be a number")
	assert(is_callable(f), "2nd arg supplied to timer:after must be callable (function or callable table)")
	self:reset()
	self.delay = delay
	self.action = f
	self.args = {...}
	return self
end

function timer:every(period, f, ...)
	assert(type(period) == 'number', "First arg supplied to timer:every() must be a number (and >= 0)")
	assert(period >= 0, "First arg supplied to timer:every() must be >= 0 (positive)")
	assert(is_callable(f), "2nd arg supplied to timer:every must be callable (function or callable table)")
	self:reset()
	self.period = period
	self.action = f
	self.args = {...}
	return self
end

function timer:update(dt)
	assert(type(dt) == 'number', "First arg supplied to timer:update(dt) must be a number")
	local dt = dt
	self.recent_trigger_count = 0
	self.recent_undo_count = 0
	
	if self.period then
		self.elapsed = self.elapsed + dt
		while math.abs(self.elapsed) >= math.abs(self.period) do
			if self.elapsed < 0 then
				self:undo()
				self.recent_undo_count = self.recent_undo_count + 1
			else
				self:trigger()
				self.recent_trigger_count = self.recent_trigger_count + 1
			end
			self.elapsed = self.elapsed - math.sign(self.elapsed) * self.period
		end
	else
		self.elapsed = self.elapsed + dt
		if not self.done and math.abs(self.elapsed) >= math.abs(self.delay) then
			self.recent_trigger_count = 1
			self.done = true
			self:trigger()
		elseif self.done and math.abs(self.elapsed) < math.abs(self.delay)  then
			self.recent_undo_count = 1
			self.done = nil
			self:undo()
		end
	end
	
	return self
end

function timer:trigger(...)
	assert(is_callable(self.action), "timer:trigger was called before setting an action via timer:after/every")
	local new_args = {...}
	if (next(new_args)) then
		self.action((unpack or table.unpack)(new_args))
	else
		self.action((unpack or table.unpack)(self.args))
	end
	self.trigger_count = self.trigger_count + 1
	return self
end

function timer:undo(...)
	assert(is_callable(self.undo_action), "timer:undo was called before setting an undo action via timer:setundo(f, ...)")
	local new_args = {...}
	if (next(new_args)) then
		self.undo_action((unpack or table.unpack)(new_args))
	else
		self.undo_action((unpack or table.unpack)(self.undo_args))
	end
	self.undo_count = self.undo_count + 1
	return self
end

function timer:reset()
	self.elapsed = 0
	self.trigger_count = 0
	self.recent_trigger_count = 0
	self.undo_count = 0
	self.recent_undo_count = 0
	self.done = nil
	return self
end

function timer:setundo(f, ...)
	assert(is_callable(f), "First arg supplied to timer:setundo must be callable (function or callable table)")
	self.undo_action = f
	self.undo_args = {...}
	return self
end

return timer