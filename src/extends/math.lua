----------------------------
-- extensions for lua math library
----------------------------

local math_abs = math.abs
local math_sqrt = math.sqrt
local math_atan2 = math.atan2 or math.atan

math.clamp = function(x, min, max)
	return x < min and min or (x > max and max or x)
end

math.round = function(num, idp)
	local sign, num = math.sign(num), math.abs(num)
	local mult = 10 ^ (idp or 0)
	return math.floor(num * mult + 0.5) / mult * sign
end

math.sign = function(x)
	return x > 0 and 1 or x < 0 and -1 or 0
end

math.lerp = function(a, b, amount)
  return a + amount * (b - a)
end

math.smooth = function(a, b, amount)
  local t = math.clamp(amount, 0, 1)
  local m = t * t * (3 - 2 * t)
  return a + (b - a) * m
end

math.pingpong = function(x)
  return 1 - math_abs(1 - x % 2)
end

math.distance = function(x1, y1, x2, y2)
	return math_sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1))
end

math.angle = function(x1, y1, x2, y2)
  return math_atan2(y2 - y1, x2 - x1)
end

math.vector = function(angle, magnitude)
  return math.cos(angle) * magnitude, math.sin(angle) * magnitude
end

math.getDirection = function(up, down, left, right)
	local x, y = 0, 0
	if up then
		y = y + 1
	end
	if down then
		y = y - 1
	end
	if left then
		x = x - 1
	end
	if right then
		x = x + 1
	end
	return x, y
end

math.collision = {}

local min, max = math.min, math.max

math.collision.pointInRectangle = function(point_x, point_y, rect_left, rect_top, rect_width, rect_height)
	local x, y, l, t, w, h = point_x, point_y, rect_left, rect_top, rect_width, rect_height
	if x >= l and x <= l + w and y >= t and y <= t + h then
		return true
	end
end

math.collision.rayIntersectsAABB = function(
	rx, ry, rz,
	dx, dy, dz,
	minx, miny, minz,
	maxx, maxy, maxz
)
	--[[
		rx, ry, rz is the ray position
		dx, dy, dz is the direction vector
		min/max xyz are the aabb bounds
	]]
	local idx, idy, idz
	if dx == 0 then
		idx = math.huge
	else
		idx = 1/dx
	end
	if dy == 0 then
		idy = math.huge
	else
		idy = 1/dy
	end
	if dz == 0 then
		idz = math.huge
	else
		idz = 1/dz
	end
	
	
	local t1 = (minx - rx) * idx
	local t2 = (maxx - rx) * idx
	local t3 = (miny - ry) * idy
	local t4 = (maxy - ry) * idy
	local t5 = (minz - rz) * idz
	local t6 = (maxz - rz) * idz

	local tmin = max(max(min(t1, t2), min(t3, t4)), min(t5, t6))
	local tmax = min(min(max(t1, t2), max(t3, t4)), max(t5, t6))

	-- ray is intersecting AABB, but whole AABB is behind us
	if tmax < 0 then
		return false
	end

	-- ray does not intersect AABB
	if tmin > tmax then
		return false
	end

	-- Return collision point and distance from ray origin
	local cx = rx + dx * tmin
	local cy = ry + dy * tmin
	local cz = rz + dz * tmin
	return cx, cy, cz, tmin
end

math.quaternionFromAxisAngle = function(theta, x, y, z)
	local q0 = math.cos(theta / 2)
	local q1 = x * math.sin(theta / 2)
	local q2 = y * math.sin(theta / 2)
	local q3 = z * math.sin(theta / 2)
	
	return q0, q1, q2, q3
end

math.rotatePoint = function(p1, p2, p3, q0, q1, q2, q3)
	-- convert point to quaternion (x, y, z) -> (0, x, y, z) = (p0, p1, p2, p3)
	local p0 = 0
	
	-- calculate inverse of rotation quaternion q (q0, q1, q2, q3) -> (q0, -q1, -q2, -q3) = (qi0, qi1, qi2, qi3)
	local qi0, qi1, qi2, qi3 = q0, -q1, -q2, -q3
	
	-- perform rotation (quaternion rotation t = r x s where t is product of mult'ing r and s quaternions)
	-- active: p' = q^-1 * p * q
	-- passive: p' = q * p * q^-1
	
	-- a represents product of q^-1 and p
	local a0 = qi0*p0-qi1*p1-qi2*p2-qi3*p3
	local a1 = qi0*p1+qi1*p0-qi2*p3+qi3*p2
	local a2 = qi0*p2+qi1*p3+qi2*p0-qi3*p1
	local a3 = qi0*p3-qi1*p2+qi2*p1+qi3*p0
	
	-- calculate p' values of x, y, z -- (p' = a * q)
	local x, y, z
	x = a0*q1+a1*q0-a2*q3+a3*q2
	y = a0*q2+a1*q3+a2*q0-a3*q1
	z = a0*q3-a1*q2+a2*q1+a3*q0
	
	return x, y, z
end

math.rotatePointAbout = function(p1, p2, p3, q0, q1, q2, q3, ox, oy, oz)
	-- ox, oy, oz are the origin offsets we're rotating about
	-- convert point to quaternion (x, y, z) -> (0, x, y, z) = (p0, p1, p2, p3)
	local p0, p1, p2, p3 = 0, p1 - ox, p2 - oy, p3 - oz
	
	-- calculate inverse of rotation quaternion q (q0, q1, q2, q3) -> (q0, -q1, -q2, -q3) = (qi0, qi1, qi2, qi3)
	local qi0, qi1, qi2, qi3 = q0, -q1, -q2, -q3
	
	-- perform rotation (quaternion rotation t = r x s where t is product of mult'ing r and s quaternions)
	-- active: p' = q^-1 * p * q
	-- passive: p' = q * p * q^-1
	
	-- a represents product of q^-1 and p
	local a0 = qi0*p0-qi1*p1-qi2*p2-qi3*p3
	local a1 = qi0*p1+qi1*p0-qi2*p3+qi3*p2
	local a2 = qi0*p2+qi1*p3+qi2*p0-qi3*p1
	local a3 = qi0*p3-qi1*p2+qi2*p1+qi3*p0
	
	-- calculate p' values of x, y, z -- (p' = a * q)
	local x, y, z
	x = a0*q1+a1*q0-a2*q3+a3*q2
	y = a0*q2+a1*q3+a2*q0-a3*q1
	z = a0*q3-a1*q2+a2*q1+a3*q0
	
	return ox + x, oy + y, oz + z
end

math.cubicBezier = function(p1x, p1y, p2x, p2y, p3x, p3y, p4x, p4y, t)
	local x, y
	local tt = 1 - t
	x = math.pow(tt, 3)*p1x + 3 * t * math.pow(tt, 2) * p2x + 3 * math.pow(t, 2) * tt * p3x + math.pow(t, 3) * p4x
	y = math.pow(tt, 3)*p1y + 3 * t * math.pow(tt, 2) * p2y + 3 * math.pow(t, 2) * tt * p3y + math.pow(t, 3) * p4y
	return x, y
end