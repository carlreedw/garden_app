----------------------------
-- extensions for lua _G
----------------------------

local ripairs_iter = function(t, i)
	i = i - 1
	local v = t[i]
	if v ~= nil then 
		return i, v
	end
end

function ripairs(t)
	return ripairs_iter, t, (#t + 1)
end

