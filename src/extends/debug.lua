----------------------------
-- extensions for lua debug library
----------------------------

debug.on = true
debug.events = {}
debug.info = {}
debug.font = love.graphics.newFont(14)
debug.color = {1, 1, 1}
debug.escape_key = 'escape'
debug.debug_key = 'f10'

function debug.listen(bool)
	-- game.debug.events will hold listeners for stuff like 'press escape to quit', window resizing, debug printing, etc
	local dbg = debug
	if bool then
		-- build events table and register event handlers (listeners)
		dbg.events.kp = events('keypressed', function(key)
			if key == dbg.escape_key then
				love.event.quit()
			elseif key == dbg.debug_key then
				debug.debug()
			end
		end)
		
		dbg.events.kp:register()
	else
		dbg.events.kp:remove()
	end
end

debug.log = function(txt)
	love.filesystem.append('log.txt', (txt or "") .. '\n')
end

debug.tostring = function(val, keep_mt_or_process_f)
	-- prints keys and contents (abridged) for any given table
	local f = function(item, path)
		if path[#path] ~= inspect.METATABLE and not keep_mt then
			return item
		end
	end
	
	if not keep_mt_or_process_f then
		return inspect(val, {indent = "	", process = f})
	end
	
	if type(keep_mt_or_process_f) ~= 'function' then
		return inspect(val, {indent = "	"})
	else
		return inspect(val, {indent = "	", process = keep_mt_or_process_f})
	end
end

debug.report = function(clr)
	-- don't report to screen unless debug.on is set
	if not debug.on then return end
	
	local clr = clr or debug.color
	love.graphics.setColor(clr)
	love.graphics.setFont(debug.font)
	
	local y = 20
	for name, val in pairs(debug.info) do
		local s = debug.tostring(val)
		love.graphics.print(tostring(name) .. ': ' .. s, 20, y)
		local newlines = 1
		for _ in s:gmatch("%\n") do newlines = newlines + 1 end
		y = y + newlines * 14
	end
end

debug.trace = function(...)
	local info = debug.getinfo(2, "Sl")
	local t = {info.short_src .. ":" .. info.currentline .. ":"}
	for i = 1, select("#", ...) do
		local x = select(i, ...)
		if type(x) == "number" then
			x = string.format("%g", math.round(x, 2))
		end
		t[#t + 1] = tostring(x)
	end
	return table.concat(t, " ")
end

debug.hotswap = function(modname)
	local oldglobal = table.clone(_G)
	local updated = {}
	local function update(old, new)
		if updated[old] then return end
		updated[old] = true
		local oldmt, newmt = getmetatable(old), getmetatable(new)
		if oldmt and newmt then update(oldmt, newmt) end
		for k, v in pairs(new) do
			if type(v) == "table" then update(old[k], v) else old[k] = v end
		end
	end
	local err = nil
	local function onerror(e)
		for k in pairs(_G) do _G[k] = oldglobal[k] end
		err = string.trim(e)
	end
	local ok, oldmod = pcall(require, modname)
	oldmod = ok and oldmod or nil
	xpcall(function()
		package.loaded[modname] = nil
		local newmod = require(modname)
		if type(oldmod) == "table" then update(oldmod, newmod) end
		for k, v in pairs(oldglobal) do
			if v ~= _G[k] and type(v) == "table" then
				update(v, _G[k])
				_G[k] = v
			end
		end
	end, onerror)
	package.loaded[modname] = oldmod
	if err then return nil, err end
	return oldmod
end

debug.write_log = function(log_file_name, log_file_contents)
	-- (over)write log file with contents
	assert(type(log_file_name) == 'string', "debug.write_log requires first arg to be (string) filename, was type: " .. type(log_file_name))
	assert(type(log_file_contents) == 'string', "debug.write_log requires second arg to be (string) file contents, was type: " .. type(log_file_contents))
	love.filesystem.remove("temp_log_files/" .. log_file_name .. ".log")
	local temp_log_file = love.filesystem.newFile("temp_log_files/" .. log_file_name .. ".log")
	
	temp_log_file:open('w')
	local success, err = temp_log_file:write(log_file_contents)
	-- error(string.format("success, error: %s, %s", success, err))
	
	temp_log_file:close()
end

love.filesystem.remove('temp_log_files')
if game.in_debug_mode then
	love.filesystem.createDirectory('temp_log_files')
end
